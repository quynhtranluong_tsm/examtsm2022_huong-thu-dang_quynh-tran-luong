import os
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats

# Read in data of part1.csv
input1 = '../Data/part1.csv'
output1 = '../Results/OutputPart1.csv'
df1 = pd.read_csv(os.path.abspath(input1), sep=',')

first = df1['FIRST_COURSE'][df1['FIRST_COURSE'] > 0]
second = df1['SECOND_COURSE'][df1['SECOND_COURSE'] > 0]
third = df1['THIRD_COURSE'][df1['THIRD_COURSE'] > 0]

# Plot the distribution of the cost per time
df1['TOTAL_COST'] = df1['FIRST_COURSE'] + df1['SECOND_COURSE'] + df1['THIRD_COURSE']
lunch = df1['TOTAL_COST'][df1['TIME'] == 'LUNCH']
dinner = df1['TOTAL_COST'][df1['TIME'] == 'DINNER']
lunch.plot.kde(label='LUNCH')
dinner.plot.kde(label='DINNER')
plt.xlabel('Cost')
plt.ylabel('Density')
plt.title('Distribution of the cost per time')
plt.grid(True)
plt.legend()
plt.savefig(os.path.abspath('../Results/CostDistributionPerTime.png'))
plt.show()

# Plot the distribution of the cost in each course
first.plot.kde()
second.plot.kde()
third.plot.kde()
plt.xlabel('Cost')
plt.ylabel('Density')
plt.title('Distribution of the cost in each course')
plt.grid(True)
plt.legend()
plt.savefig(os.path.abspath('../Results/CostDistribution.png'))
plt.show()

# Make a bar plot of the cost per course
fig, ax = plt.subplots()
n, bins, patches = ax.hist(first, 10, density=True, label='FIRST_COURSE', alpha=0.5)
n, bins, patches = ax.hist(second, 10, density=True, label='SECOND_COURSE', alpha=0.5)
n, bins, patches = ax.hist(third, 10, density=True, label='THIRD_COURSE', alpha=0.5)
y = scipy.stats.norm.pdf(bins, 0, 1)
ax.plot(bins, y, '--')
ax.set_xlabel('Cost')
ax.set_ylabel('Probability density')
ax.set_title(r'Normal distribution with $\mu=0$, $\sigma=1$')
fig.tight_layout()
plt.grid(True)
plt.legend()
plt.savefig(os.path.abspath('../Results/CostPerCourse.png'))
plt.show()


# Determine the cost of drinks per course
class Course(object):
    def __init__(self, price1, price2, price3, price4, name1, name2, name3, name4):
        assert price1 <= price2 <= price3 <= price4
        self.price1 = price1
        self.price2 = price2
        self.price3 = price3
        self.price4 = price4
        self.name1 = name1
        self.name2 = name2
        self.name3 = name3
        self.name4 = name4

    def meal(self, cost):  # Find the actual food
        if cost < self.price1:
            return ' '
        elif self.price1 <= cost < self.price2:
            return self.name1
        elif self.price2 <= cost < self.price3:
            return self.name2
        elif self.price3 <= cost < self.price4:
            return self.name3
        else:
            return self.name4

    def drink(self, cost):  # Calculate the cost of the drink
        if cost < self.price1:
            return 0
        elif self.price1 <= cost < self.price2:
            return cost - self.price1
        elif self.price2 <= cost < self.price3:
            return cost - self.price2
        elif self.price3 <= cost < self.price4:
            return cost - self.price3
        else:
            return cost - self.price4

    def cost_dish(self, dish_name):  # Find the cost of the dish (used in part 4)
        if dish_name == self.name1:
            return self.price1
        elif dish_name == self.name2:
            return self.price2
        elif dish_name == self.name3:
            return self.price3
        elif dish_name == self.name4:
            return self.price4


course1 = Course(0, 3, 15, 20, ' ', 'Soup', 'Tomato-Mozarella', 'Oysters')
course2 = Course(9, 20, 25, 40, 'Salad', 'Spaghetti', 'Steak', 'Lobster')
course3 = Course(0, 0, 10, 15, ' ', ' ', 'Pie', 'Ice cream')

food_c1 = []
drink_cost_c1 = []
food_c2 = []
drink_cost_c2 = []
food_c3 = []
drink_cost_c3 = []

for i in range(len(df1)):
    food_c1.append(course1.meal(df1.iloc[i, 2]))
    drink_cost_c1.append(course1.drink(df1.iloc[i, 2]))
    food_c2.append(course2.meal(df1.iloc[i, 3]))
    drink_cost_c2.append(course2.drink(df1.iloc[i, 3]))
    food_c3.append(course3.meal(df1.iloc[i, 4]))
    drink_cost_c3.append(course3.drink(df1.iloc[i, 4]))

df1['FOOD_C1'] = food_c1
df1['DRINK_COST_C1'] = drink_cost_c1
df1['FOOD_C2'] = food_c2
df1['DRINK_COST_C2'] = drink_cost_c2
df1['FOOD_C3'] = food_c3
df1['DRINK_COST_C3'] = drink_cost_c3
print(df1)

df1.to_csv(os.path.abspath(output1), sep=',', index=False)
