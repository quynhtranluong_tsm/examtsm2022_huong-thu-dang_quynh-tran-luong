import os
import numpy as np
import pandas as pd
import random
import Part3_DeterminingDistributions as p3
from Part1_ExploratoryDataAnalysis import Course

list_choose = ['Eat', 'Not eat']  # Used in the function to draw the probability of eating first, second, third course

# Create a list of ID of returning customers, from ID100000 to ID 100999
list_ID_returning = []
for i in range(0, 1000):
    list_ID_returning.append('ID' + str(100000 + i))


# Function to draw random dish based on probability
class Customer(object):
    def __init__(self, name1, name2, name3, name4):
        self.name1 = name1
        self.name2 = name2
        self.name3 = name3
        self.name4 = name4

    # Create the list of choosing 'eat'/'not eat' of the first, second and third course
    def course_per_client_type(self, customer_type):
        list_course_eat = []
        course_col = [col for col in p3.course.columns if customer_type in col]
        for i in range(len(p3.list_type)):
            if course_col == p3.list_type[i].split():
                for j in range(len(p3.list_course)):
                    list_course_eat.append(np.random.choice(list_choose,
                                                            p=(p3.list_course[j][i], 1 - p3.list_course[j][i])))
        return list_course_eat

    # For the 3 functions goToRestaurant() below, the probability is the list of probability of a specific customer type
    # This is a separate function as it depends on the random choice of eating or not in the first course
    def goToRestaurant_FirstCourse(self, customer_type):
        dish_col = [col for col in p3.dish.columns if customer_type in col]
        for i in range(len(p3.list_type)):
            if dish_col == p3.list_type[i].split():
                return np.random.choice(p3.list_meal[0],
                                        p=(p3.list_dish[0][i], p3.list_dish[0][i + 4], p3.list_dish[0][i + 8]))

    # This is a separate function as it depends on the random choice of eating or not in the second course
    def goToRestaurant_SecondCourse(self, customer_type):
        dish_col = [col for col in p3.dish.columns if customer_type in col]
        for i in range(len(p3.list_type)):
            if dish_col == p3.list_type[i].split():
                return np.random.choice(p3.list_meal[1], p=(p3.list_dish[1][i], p3.list_dish[1][i + 4],
                                                            p3.list_dish[1][i + 8], p3.list_dish[1][i + 12]))

    # This is a separate function as it depends on the random choice of eating or not in the third course
    def goToRestaurant_ThirdCourse(self, customer_type):
        dish_col = [col for col in p3.dish.columns if customer_type in col]
        for i in range(len(p3.list_type)):
            if dish_col == p3.list_type[i].split():
                return np.random.choice(p3.list_meal[2], p=(p3.list_dish[2][i], p3.list_dish[2][i + 4]))

    # The function ChooseID() chooses ID based on customer type
    # The ID is chosen from the returning data for a returning customer or created for a new customer
    def ChooseID(self, customer_type):
        if customer_type == self.name1:
            return np.random.choice(
                (random.choices(list_ID_returning[0:333]), 'ID' + str(random.sample(range(101000, 201000), 1))),
                p=(0.5, 0.5))
        if customer_type == self.name2:
            return np.random.choice(
                (random.choices(list_ID_returning[333:666]), 'ID' + str(random.sample(range(201000, 301000), 1))),
                p=(0.7, 0.3))
        if customer_type == self.name3:
            return 'ID' + str(random.sample(range(301000, 401000), 1))
        if customer_type == self.name4:
            return np.random.choice(
                (random.choices(list_ID_returning[666:1000]), 'ID' + str(random.sample(range(401000, 501000), 1))),
                p=(0.9, 0.1))


customer = Customer('Business', 'Healthy', 'Onetime', 'Retirement')
# Check some results
print(customer.goToRestaurant_FirstCourse('Healthy'))
print(customer.goToRestaurant_FirstCourse('Business'))
print(customer.course_per_client_type('Healthy'))
print(customer.ChooseID('Retirement'))

course1 = Course(0, 3, 15, 20, ' ', 'Soup', 'Tomato-Mozarella', 'Oysters')
course2 = Course(9, 20, 25, 40, 'Salad', 'Spaghetti', 'Steak', 'Lobster')
course3 = Course(0, 0, 10, 15, ' ', ' ', 'Pie', 'Ice cream')

# Simulation to create restaurant data
list_ID = []
food_c1 = []
drink_c1 = []
drink_c2 = []
drink_c3 = []
food_c2 = []
food_c3 = []
total1 = []
total2 = []
total3 = []

# Draw the dataset used for returning customer
df = pd.DataFrame()
N = 5 * 365 * 20  # 5 years of exactly 365 days and 20 courses per day
df['TIME'] = np.random.choice(p3.list_time, p=(0.5, 0.5), size=N)
for i in range(0, N):
    if df.iloc[i, 0] == 'LUNCH':
        df['CLIENT_TYPE'] = np.random.choice(p3.list_type, p=p3.list_type_per_time[0], size=N)  # Choose customer type
        list_ID.append(customer.ChooseID(df.iloc[i, 1]))
    else:
        df['CLIENT_TYPE'] = np.random.choice(p3.list_type, p=p3.list_type_per_time[1], size=N)  # Choose customer type
        list_ID.append(customer.ChooseID(df.iloc[i, 1]))

    # Choose the first course's dish
    # df.iloc[i, 1] indicates CLIENT_TYPE, [0] indicates the first course's position in the result of list_course_eat
    if (customer.course_per_client_type(df.iloc[i, 1])[0]) == 'Eat':
        food_c1.append(customer.goToRestaurant_FirstCourse(
            df.iloc[i, 1]))  # Choose dish, the drawing does not align with the probability
        drink_c1.append(round(random.uniform(0.01, 2.00), 2))
        total1.append(course1.cost_dish(food_c1[i]) + float(drink_c1[i]))
    else:
        food_c1.append('Not eat')
        drink_c1.append(0)
        total1.append(0)

    if (customer.course_per_client_type(df.iloc[i, 1])[1]) == 'Eat':  # Choose second course dish
        food_c2.append(customer.goToRestaurant_SecondCourse(df.iloc[i, 1]))
        drink_c2.append(round(random.uniform(0.01, 5.00), 2))
        total2.append(course2.cost_dish(food_c2[i]) + float(drink_c2[i]))
    else:
        food_c2.append('Not eat')
        drink_c2.append(0)
        total2.append(0)

    if (customer.course_per_client_type(df.iloc[i, 1])[2]) == 'Eat':  # Choose third course dish
        food_c3.append(customer.goToRestaurant_ThirdCourse(df.iloc[i, 1]))
        drink_c3.append(round(random.uniform(0.01, 2.00), 2))
        total3.append(course3.cost_dish(food_c3[i]) + float(drink_c3[i]))
    else:
        food_c3.append('Not eat')
        drink_c3.append(0)
        total3.append(0)

df['CLIENT_ID'] = list_ID
df['FOOD_C1'] = food_c1
df['FOOD_C2'] = food_c2
df['FOOD_C3'] = food_c3
df['DRINK_C1'] = drink_c1
df['DRINK_C2'] = drink_c2
df['DRINk_C3'] = drink_c3
df['TOTAL1'] = total1
df['TOTAL2'] = total2
df['TOTAL3'] = total3

output4 = '../Results/OutputPart4.csv'
df.to_csv(os.path.abspath(output4), sep=',', index=False)
