import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D

# Read in data
input1 = '../Data/part1.csv'
df1 = pd.read_csv(os.path.abspath(input1), sep=',')

df2 = df1[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']]
X = df2.values
kmeans_optimum = KMeans(n_clusters=4, init='k-means++', random_state=100)
y = kmeans_optimum.fit_predict(X)
print(y)
df1['cluster'] = y
data1 = df1[df1.cluster == 0]
data2 = df1[df1.cluster == 1]
data3 = df1[df1.cluster == 2]
data4 = df1[df1.cluster == 3]

kplot = plt.axes(projection='3d')
xline = np.linspace(0, 15, 1000)
yline = np.linspace(0, 15, 1000)
zline = np.linspace(0, 15, 1000)

kplot.plot3D(xline, yline, zline, 'black')
kplot.scatter3D(data1.FIRST_COURSE, data1.SECOND_COURSE, data1.THIRD_COURSE, c='red', label='Cluster 1')
kplot.scatter3D(data2.FIRST_COURSE, data2.SECOND_COURSE, data2.THIRD_COURSE, c='green', label='Cluster 2')
kplot.scatter3D(data3.FIRST_COURSE, data3.SECOND_COURSE, data3.THIRD_COURSE, c='blue', label='Cluster 3')
kplot.scatter3D(data4.FIRST_COURSE, data4.SECOND_COURSE, data4.THIRD_COURSE, c='yellow', label='Cluster 4')
kplot.w_xaxis.set_ticklabels([])
kplot.w_yaxis.set_ticklabels([])
kplot.w_zaxis.set_ticklabels([])
kplot.set_xlabel('FIRST_COURSE')
kplot.set_ylabel('SECOND_COURSE')
kplot.set_zlabel('THIRD_COURSE')
kplot.set_title('Ground Truth')
plt.legend()
plt.savefig(os.path.abspath('../Results/Clustering.png'))
plt.show()


class ClientType(object):
    def __init__(self, type1, type2, type3, type4):
        self.type1 = type1
        self.type2 = type2
        self.type3 = type3
        self.type4 = type4

    def client(self, cluster):  # Predict client type based on cluster
        if cluster == 0:
            return self.type1
        elif cluster == 1:
            return self.type2
        elif cluster == 2:
            return self.type3
        else:
            return self.type4


predict = ClientType('Retirement', 'Onetime', 'Business', 'Healthy')
client_c1 = []
for i in range(len(df1)):
    client_c1.append(predict.client(df1.iloc[i, -1]))

df1['CLIENT_PREDICT'] = client_c1
print(df1)

output2 = '../Results/OutputPart2.csv'
df1.to_csv(os.path.abspath(output2), sep=',', index=False)
