import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Read in data and merge csv files
input3 = '../Data/part3.csv'
output1 = '../Results/OutputPart1.csv'
output2 = '../Results/OutputPart2.csv'
merged = '../Results/Merged.csv'
df3 = pd.read_csv(os.path.abspath(input3), sep=',')
df2_output = pd.read_csv(os.path.abspath(output2), sep=',')
df1_output = pd.read_csv(os.path.abspath(output1), sep=',')
df_merged = pd.merge(df2_output, df3, how='outer', on='CLIENT_ID')
cols_to_use = df1_output.columns.difference(df_merged.columns)
df_merged = pd.merge(df_merged, df1_output[cols_to_use], left_index=True, right_index=True, how='outer')
df_merged['ACCURATE'] = np.where(df_merged['CLIENT_PREDICT'] == df_merged['CLIENT_TYPE'], True, False)
df_merged.to_csv(os.path.abspath(merged), sep=',', index=False)

# Check the accuracy of the prediction
percent_accurate = df_merged.ACCURATE.value_counts(normalize=True).mul(100).round(1).astype(str) + '%'
prob_accurate = pd.DataFrame({'ACCURATE': percent_accurate})
print(prob_accurate)

# Determine the distribution of clients
percent_client_dis = df_merged.CLIENT_TYPE.value_counts(normalize=True).mul(100).round(1).astype(str) + '%'
client_dis = pd.DataFrame({'CLIENT_DIST': percent_client_dis})
print(client_dis)

# Answer the question "What type of client is this?"
ID = input("What is the client ID?")
for i in range(len(df_merged)):
    if df_merged.iloc[i, 0] == ID:
        print(df_merged.iloc[i, 7])

# Determine the likelihood for each of these clients to get a certain course
list_type = ['Business', 'Healthy', 'Onetime', 'Retirement']
list_course_name = ['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']
list_course = [[], [], []]
for i in range(2, 5):
    for j in range(len(list_type)):
        a = df_merged[(df_merged.iloc[:, i] > 0) & (df_merged.CLIENT_TYPE == list_type[j])].count()[i]
        b = df_merged[(df_merged.CLIENT_TYPE == list_type[j])].count()[i]
        list_course[i - 2].append(round(a / b, 4))
course = pd.DataFrame(list_course, columns=list_type, index=list_course_name)
print(course)


# Determine the probability of a certain type of customer ordering a certain dish
# The following function is applied to a specific case in which a list contains 3 sub-lists with different lengths only
def sublist(biglist):
    list0 = [biglist[0][l:l + 4] for l in range(0, len(biglist[0]), 4)]
    list1 = [biglist[1][l:l + 4] for l in range(0, len(biglist[1]), 4)]
    list2 = [biglist[2][l:l + 4] for l in range(0, len(biglist[2]), 4)]
    list_total = list0 + list1 + list2
    return list_total


list_meal = [['Soup', 'Tomato-Mozarella', 'Oysters'], ['Salad', 'Spaghetti', 'Steak', 'Lobster'], ['Pie', 'Ice cream']]
list_dish = [[], [], []]
for i in range(len(list_meal)):
    for j in range(len(list_meal[i])):
        for k in range(len(list_type)):
            a = df_merged[(df_merged.CLIENT_TYPE == list_type[k]) &
                          (df_merged.iloc[:, i - 5] == list_meal[i][j])].count()[i - 5]
            b = df_merged[(df_merged.CLIENT_TYPE == list_type[k]) & (df_merged.iloc[:, i - 5] != ' ')].count()[i - 5]
            list_dish[i].append(round(a / b, 4))
dish = pd.DataFrame(sublist(list_dish), columns=list_type, index=[sub for sublist in list_meal for sub in sublist])
print(dish)

# Determine the distribution of dishes per course
list_dish_per_course = [[], [], []]
for i in range(len(list_meal)):
    for j in range(len(list_meal[i])):
        a = df_merged[(df_merged.iloc[:, i - 5] == list_meal[i][j])].count()[i - 5]
        b = df_merged[(df_merged.iloc[:, i - 5] != ' ')].count()[i - 5]
        list_dish_per_course[i].append(list_meal[i][j] + ': ' + str(round(a / b, 4)))
dish_per_course = pd.DataFrame(list_dish_per_course, index=list_course_name)
print(dish_per_course)

# Determine the distribution of dishes per customer type
list_dish_per_customer = [[], [], []]
for i in range(len(list_meal)):
    for j in range(len(list_meal[i])):
        for k in range(0, 4):
            a = df_merged[(df_merged.CLIENT_TYPE == list_type[k]) &
                          (df_merged.iloc[:, i - 5] == list_meal[i][j])].count()['CLIENT_TYPE']
            b = df_merged[(df_merged.iloc[:, i - 5] == list_meal[i][j])].count()['CLIENT_TYPE']
            list_dish_per_customer[i].append(round(a / b, 4))
dish_per_customer = pd.DataFrame(sublist(list_dish_per_customer), columns=list_type,
                                 index=[sub for sublist in list_meal for sub in sublist])
print(dish_per_customer)

# Determine the distribution of the cost of the drinks per course
drink1 = df_merged['DRINK_COST_C1'][df_merged['DRINK_COST_C1'] > 0]
drink2 = df_merged['DRINK_COST_C2'][df_merged['DRINK_COST_C2'] > 0]
drink3 = df_merged['DRINK_COST_C3'][df_merged['DRINK_COST_C3'] > 0]

drink1.plot.kde()
drink2.plot.kde()
drink3.plot.kde()
plt.xlabel('Drink cost')
plt.ylabel('Density')
plt.title('Distribution of the cost of the drinks per course')
plt.grid(True)
plt.legend()
plt.savefig(os.path.abspath('../Results/DrinkCostDistribution.png'))
plt.show()

# Determine the distribution of different types of customers per time: This additional sub-part is for part 4
list_time = ['LUNCH', 'DINNER']
list_type_per_time = [[], []]
for i in range(len(list_time)):
    for j in range(len(list_type)):
        a = df_merged[(df_merged.TIME == list_time[i]) & (df_merged.CLIENT_TYPE == list_type[j])].count()['CLIENT_TYPE']
        b = df_merged[(df_merged.TIME == list_time[i])].count()['CLIENT_TYPE']
        list_type_per_time[i].append(a / b)
type_per_time = pd.DataFrame(list_type_per_time, columns=list_type, index=list_time)
print(type_per_time)
