## Part 1: Exploratory Data Analysis

1. Make a plot of cost distribution by time:
- Calculate the total cost 
- Using plot.kde to make the line graph with distribution (CourseDistributionPerTime.png)
2. Make a barplot of cost per courses:
- Using plot.kde to make the line graph with distribution(Cost Distribution)
- Using ax.hist with bins = 10 and alpha = 0.5 for transparency 
3. Find the actual dish and cost drink
- Create Class object to store the information of the course (include 1 null value for first course and 2 null values for second course)
- Build a function to determine the dish price and dish name: 
  + Compare the cost in range(price dish 1 <= cost< price dish 2 )
  + If accepted, then the cost of the dish will be the lower bound of the range
  + The dish name is corresponding to the lower bound price
- Build the function to determine drink cost: 
  + Drink cost = Total cost - dish cost
- Create lists and append the results of loop calculating throughout each row of original dataframe.
- Append it into new columns of dataframe   

---

## Part 2: Clustering Data

1. Create subset dataframe includes three columns of cost(df2)
2. Use .value to get the right format out of the dataframe and call it X  
3. Use KMeans to build the structure for clustering with 4 clusters 
4. Variable Y is the prediction when apply MMeans function into X
5. Plot data in 3D graph
6. Label each cluster in the data by Class object with naming function
7. Create lists and append the results of loop calculating throughout each row of original dataframe.
8. Append it into new columns of original dataframe(df1)
9. Specific characteristics per group: 
  + Business: mostly order three course meal in which for second course the dish cost always highest in all sample(higher then $40) - all dishes ordered are lobster.  
  + Healthy: mostly order first and second course which the price is max around $10 for first course and around $13 for second course - all dishes ordered are soup and salad while hardly ordered dessert.
  + Onetime: mostly just order second course with the price varied from around 20 to around 40  
  + Retirement: the majority order three-course meal but the price for second-course dish is not as high as Business client (never order Lobster) and varies for all dishes in first course and third course.   

---

## Part 3: Determine Distribution

1. Read in input data as well as output data from part 1 and 2, then merge them to a new file.
2. Check the accuracy of the prediction in part 2 with the given dataset by creating a new column **ACCURATE** taking value **True** if the result is the same as that of the original data file. The calculation of the probability of this **True** value gives the proportion of accuracy of clustering.
3. To answer the question "What type of client is this?", create an input box asking the user to enter a client ID then search it in column **CLIENT_ID** by using for loop. In case of success, return the corresponding client type, otherwise nothing will happen.
4. Determine different probability distribution, such as the likelihood for each client type to get a certain course/dish, the distribution of dish per course/client type, the distribution of each client type per time:
   - Create **list_type**, **list_course_name**, **list_meal**, **list_time** consisting of the list of customer type, course name, dish name and time, respectively. Note that **list_meal** is a special list because it contains 3 sub-lists with different length, each of them corresponds to a certain course.
   - Create an empty list to add elements later. This empty list will be a column of a dataframe.
   - Count values for the numerator and denominator, denoted by **a** and **b** respectively, by combining the for loop with conditionals.
   - Append the probability value **a / b** to the empty list, taking 4 decimals.
   - Create a new dataframe using this list to show the final result and print it after that.
5. Determine the distribution of the drink cost per course by using plot.kde to make a line graph. Only take into account drink cost that is higher than 0.

---

## Part 4: Simulation
**Support Functions**
1. Create list of returning ID. 
2. Build functions to draw variables based on Customer Type( we will call the input for clarification): 
   - Create Class object to store customer type 
   - Functions to draw the probability to eat or not eat the course:( link with the probability of ordering a certain course -**list_course** list and **course** dataframe in part 3)
     + Check the input equals to which columns named in the **course** dataframe
     + If the input equals to one element in **list_type**(list of client type) then looping for all the **list_course** choose between eat/ not eat by the probability presented in **list_course**
   - For the 3 functions goToRestaurant() below, the probability is the list of probability of a specific customer type
     .This is a separate function as because it is independent probability for each dish in different course
     + Link with the probability of ordering a certain dish -**list_dish** list and **dish** dataframe in part 3)
     + Check the input equals to which column named in the **dish** dataframe
     + If the input equals to one element in **list_type**(list of client type) then looping for all the **list_meal** choose between each dish by the probability presented in **list_dish** 
     + Noted that: we have to specify the position of element in list_meal and list_dish in each course.
3. Build functions to draw ID return and ID new: 
     + Based on the customer type, choose with probability to take the ID in returning list in 1. or create new ID for new customer.
     + Note 1: returning list only contains ID for Business, Healthy and Retirement with specify the number range for each group.
     + Note 2: new IDs are generated with the specific number range for each type of Client(including the Onetime group )                   
 
**Simulation**   
- Build each column by each column by appending the list generated from above functions.
- Generate Time -> Client Type -> Client ID -> Eat/ Not Eat specific course -> Dish order if course is eated -> Cost drink randomly choose based on the cost drink distribution in part 3 -> Total cost by the sum of dish price and drink cost.
- Append the lists into dataframe. 